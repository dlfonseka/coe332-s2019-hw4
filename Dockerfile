FROM python:3-onbuild
RUN apt-get update
EXPOSE 5000
CMD ["python", "./main.py"]
